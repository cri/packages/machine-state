{
  description = "Dbus object holding the computer state of an epita's computer.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, poetry2nix, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay poetry2nix.overlay ];
      });

      poetryArgs = pkgs: {
        projectDir = self;
        src = self;

        postInstall = ''
          install -Dm644 $src/org.cri.MachineState.conf $out/etc/dbus-1/system.d/org.cri.MachineState.conf
        '';

        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
          pycairo = super.pycairo.overridePythonAttrs (old: {
            preBuild = ''
              cd ../
            '';
            postBuild = ''
              cd build
            '';
          });
          pygobject = super.pygobject.overridePythonAttrs (old: {
            buildInputs = old.buildInputs ++ [ self.setuptools ];
          });
          black = null;
          pre-commit = null;
          virtualenv = null;
        });

        meta = with lib; {
          inherit (self) description;
          maintainers = with maintainers; [ risson ];
        };
      };

      anySystemOutputs = {
        overlay = final: prev: {
          machine-state = final.poetry2nix.mkPoetryApplication (poetryArgs final);
        };
      };

      multipleSystemsOutputs = eachDefaultSystem (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = with pkgs; [
              git
              nixpkgs-fmt
              poetry
              (pkgs.poetry2nix.mkPoetryEnv (removeAttrs (poetryArgs pkgs) [ "meta" "src" "postInstall" ]))
              pre-commit
            ];
          };

          packages = {
            inherit (pkgs) machine-state;
          };
          defaultPackage = self.packages.${system}.machine-state;

          apps = {
            machine-state = {
              type = "app";
              program = "${self.defaultPackage.${system}}/bin/machine-state";
            };
          };
          defaultApp = self.apps.${system}.machine-state;
        });
    in
    recursiveUpdate multipleSystemsOutputs anySystemOutputs;
}
