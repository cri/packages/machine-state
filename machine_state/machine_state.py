#!/usr/bin/env python3
"""
This module exposes a python object on the system DBus to share data
between processes.
"""
import os
from enum import Enum
from functools import total_ordering
import logging
import socket
import sys
from random import randint
from time import sleep, time
import requests
from gi.repository import GLib
from pydbus import SystemBus
from pydbus.generic import signal

# Logging
LOGGER = logging.getLogger('__name__')
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')

HANDLER_DEBUG = logging.StreamHandler(sys.stdout)
HANDLER_DEBUG.setLevel(logging.DEBUG)

HANDLER_INFO = logging.StreamHandler(sys.stdout)
HANDLER_INFO.setLevel(logging.INFO)

HANDLER_ERROR = logging.StreamHandler(sys.stderr)
HANDLER_ERROR.setLevel(logging.ERROR)

LOGGER.addHandler(HANDLER_DEBUG)
LOGGER.addHandler(HANDLER_INFO)
LOGGER.addHandler(HANDLER_ERROR)

# Const Values
SESSION_PING_TIMEOUT = 30

SESSION_IGNORED_LOGINS = (
    "lightdm",
    "sddm",
)

# Led Values
# pylint: disable=comparison-with-callable
@total_ordering
class MachineStatus(Enum):
    """Enum containing all the led status."""
    POWER_OFF = 0
    POWER_ON = 1
    USER_LOGGED = 2
    USER_LOCKED = 3
    USER_LOCKED_OVERTIME = 4
    COMPUTER_HEALTHY = 5
    COMPUTER_ISSUE_UNKNOWN = 6
    COMPUTER_ISSUE_LOW = 7
    COMPUTER_ISSUE_MEDIUM = 8
    COMPUTER_ISSUE_HIGH = 9

    def __ge__(self, other):
        if isinstance(other, type(self)):
            return self.value >= other.value
        return NotImplemented
    def __gt__(self, other):
        if isinstance(other, type(self)):
            return self.value > other.value
        return NotImplemented
    def __le__(self, other):
        if isinstance(other, type(self)):
            return self.value <= other.value
        return NotImplemented
    def __lt__(self, other):
        if isinstance(other, type(self)):
            return self.value < other.value
        return NotImplemented

class Session():
    """This class stores all the session related fields."""

    def __init__(self, session_id, login, uid, update_time=SESSION_PING_TIMEOUT):
        self._id = session_id
        self._login = login
        self._uid = uid
        self._update_time = update_time
        self._creation_time = time()
        self._lock_status = (MachineStatus.USER_LOGGED, self._creation_time)
        self._lock_time = 0

    def is_a_system_session(self):
        """Check if the session belong to a system user, ex: sddm"""
        return self._uid < 1000

    def lock_ping_is_valid(self):
        """
        Return true if self._lock_status has been updated for less than self._update_time
        """
        return (time() - self._lock_status[1]) < self._update_time

    @property
    def lock_status(self):
        """Gets self._lock_status value"""
        return self._lock_status[0]

    @lock_status.setter
    def lock_status(self, status):
        """Sets self._lock_status value"""
        self._lock_status = (status, time())

    @property
    def lock_time(self):
        """Gets self._lock_time"""
        return self._lock_time

    @lock_time.setter
    def lock_time(self, new_time):
        """Sets self._lock_time value"""
        self._lock_time = new_time

    def __repr__(self):
        """Returns a serialized session"""
        return f'{self._id}, {self._login}, {self._uid}, {self.lock_status.name}, {self._lock_time}'

    def __str__(self):
        """Returns a printable string from the session attributes"""
        return f"""Session id: {self._id}, Login: {self._login}, uid: \
{self._uid}, Lock_Status: {self.lock_status.name}, LockTime: {self._lock_time}"""

class MachineState():
    """
    <node>
        <interface name="org.cri.MachineState">
            <method name='LockSession'>
                <arg type='s' name='session_id' direction='in'/>
            </method>
            <method name='OvertimeLockSession'>
                <arg type='s' name='status' direction='in'/>
            </method>
            <method name='UnlockSession'>
                <arg type='s' name='session_id' direction='in'/>
            </method>
            <method name='ListSessions'>
                <arg type='as' name='sessions' direction='out'/>
            </method>
            <method name='ForceColorSignal'></method>
            <method name='LedColorOverride'>
                <arg type='s' name='status' direction='in'/>
            </method>
            <property name="LedColor" type="s" access="read">
                <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
            </property>
        </interface>
    </node>
    """
    # pylint: disable=too-many-instance-attributes
    # Might need to store some attributes in a dict.
    def __init__(self, my_sys_bus, timeout=SESSION_PING_TIMEOUT):
        self._ip = os.getenv("MACHINE_STATE_IP")
        self._session_endpoint = os.getenv("MACHINE_STATE_SESSION_ENDPOINT")
        self._issues_endpoint = os.getenv("MACHINE_STATE_ISSUES_ENDPOINT")
        self._image = os.getenv("IMAGE")

        self._sessions = {}
        self._sys_bus = my_sys_bus
        self._led_color = (MachineStatus.POWER_ON, 0.0)
        self._led_color_overrided = None
        self._need_to_reset = False
        self._last_update = time()
        self._last_upload = 0
        self._hostname = socket.gethostname()
        self._computer_status = MachineStatus.COMPUTER_HEALTHY
        self.timeout = timeout
        GLib.timeout_add_seconds(self.timeout, self._upload_data)
        GLib.timeout_add_seconds(self.timeout, self._lock_status_heartbeat_failure)
        GLib.timeout_add_seconds(60 * 60 * 6, self.get_issues) # every 6 hours

    def _upload_data(self):
        sleep(randint(0,10))
        for _, session in self._sessions.items():
            if session.is_a_system_session() and session._login in SESSION_IGNORED_LOGINS:
                continue
            requests.post(self._session_endpoint, json={
                "ip": self._ip,
                "login": session._login,
                "image": self._image,
                "is_locked": session.lock_status == MachineStatus.USER_LOCKED,
            })
        self._update_upload_timer()
        return True

    def _update_timer(self):
        self._last_update = time()

    def _update_upload_timer(self):
        self._last_upload = time()

    # Sessions
    def add_session_callback(self, _sender, _obj, _iface, _signal, params):
        """
        Callback method: creates a new session and add it to self._sessions
        """
        session_id, session_path = params[:2]
        if session_id in self._sessions:
            logging.error("This session already exists.")
        else:
            # Get all info from DBUS Logind
            session_obj = self._sys_bus.get("org.freedesktop.login1", session_path)
            properties_obj = session_obj["org.freedesktop.DBus.Properties"]
            uid = properties_obj.Get("org.freedesktop.login1.Session", "User")[0]
            name = properties_obj.Get("org.freedesktop.login1.Session", "Name")
            self.add_session(session_id, name, uid)

    def add_session(self, session_id, name, uid):
        """Add a session."""
        self._sessions[session_id] = Session(session_id, name, uid)
        logging.info("Adding the new session...")
        logging.info(str(self._sessions[session_id]))
        self._identify_led_color()
        self._update_timer()

    def rm_session_callback(self, _sender, _obj, _iface, _signal, params):
        """
        Callback method: Removes a session from self._sessions
        """
        session_id = params[0]
        try:
            del self._sessions[session_id]
            logging.info("Removed session %s", session_id)
            self._identify_led_color()
            self._update_timer()
        except KeyError:
            logging.error("This session dosen't exists.")

    def discover_sessions(self):
        """Check if there are some existing sessions."""
        login1_obj = self._sys_bus.get("org.freedesktop.login1", "/org/freedesktop/login1")
        manager_obj = login1_obj["org.freedesktop.login1.Manager"]
        bus_sessions = manager_obj.ListSessions()
        for session_id, user_id, username, *_ in bus_sessions:
            if session_id in self._sessions:
                logging.error("This session already exists.")
            else:
                self.add_session(session_id, username, user_id)

    def _has_a_non_system_session(self):
        """Check if there is a non system session."""
        return any(not v.is_a_system_session() for v in self._sessions.values())

    def ListSessions(self):
        # pylint: disable=invalid-name
        """List sessions"""
        return [repr(session) for session in self._sessions.values()]

    # Issues
    def get_issues(self):
        """
        Fills the issues part of the MachineState object
        """
        api_url = self._issues_endpoint
        req_params = {
            'ip': self._ip,
            'status': 'confirmed',
        }
        req = requests.get(url=api_url, params=req_params)
        if req.status_code == 200:
            data = req.json()
            for issues in data['results']:
                try:
                    issue = MachineState.computer_health[issues['severity']]
                except KeyError:
                    logging.error("Invalid issue value")
                    continue
                if issue > self._computer_status:
                    self._computer_status = issue

    # Lock Status
    def LockSession(self, session_id):
        # pylint: disable=invalid-name
        """
        Lock the session_id session if it exists
        """
        if self._set_lock_status(session_id, MachineStatus.USER_LOCKED) \
                and self._sessions[session_id].lock_time == 0:
            self._sessions[session_id].lock_time = time()


    def OvertimeLockSession(self, session_id):
        # pylint: disable=invalid-name
        """
        Lock the session_id session if it exists
        """
        self._set_lock_status(session_id, MachineStatus.USER_LOCKED_OVERTIME)

    def UnlockSession(self, session_id):
        # pylint: disable=invalid-name
        """
        Unlock the session_id session if it exists
        """
        if self._set_lock_status(session_id, MachineStatus.USER_LOGGED):
            self._sessions[session_id].lock_time = 0

    def _set_lock_status(self, session_id, status):
        """
        Sets the _lock_status in the proper session if it exists
        """
        if self._sessions and session_id in self._sessions:
            self._sessions[session_id].lock_status = status
            self._identify_led_color()
            logging.info("New lock status: %s for session: %s", status.name, session_id)
            return True
        logging.error("This session is invalid.")
        return False


    def _send_color_via_signal(self):
        """Send a signal to notify the LedColor change"""
        self.PropertiesChanged("org.cri.MachineState", {"LedColor": self.LedColor}, [])

    # LED Color
    @property
    def LedColor(self):
        # pylint: disable=invalid-name
        """Gets self._led_color value"""
        return self._led_color[0]

    @LedColor.setter
    def LedColor(self, color):
        # pylint: disable=invalid-name
        previous_color = self.LedColor
        self._led_color = (color, time())
        self._update_timer()
        if previous_color != self.LedColor:
            self._send_color_via_signal()

    def _get_max_lock_status(self):
        """Return the max lock_status from all sessions"""
        max_lock = MachineStatus.USER_LOGGED
        for session in self._sessions.values():
            if session.lock_status > max_lock:
                max_lock = session.lock_status
        return max_lock

    def _get_led_color_no_session(self):
        """Get the status when there is no session"""
        status = MachineStatus.POWER_ON
        if self._computer_status >= MachineStatus.COMPUTER_ISSUE_MEDIUM:
            status = self._computer_status
        return status

    def reset_color(self):
        """Check if we need to reset the color"""
        if self._need_to_reset:
            return ""
        return None

    def is_overrided(self):
        """Check if status is overrided"""
        return self._led_color_overrided

    def has_no_sessions(self):
        """Check if there is a session"""
        if not self._sessions:
            return self._get_led_color_no_session()
        return None

    def has_session(self):
        """Get the good led status when there is a session"""
        if self._has_a_non_system_session():
        # Lock Check on all existing sessions
            return self._get_max_lock_status()
        return self._get_led_color_no_session()

    status_functions = [
        reset_color,
        is_overrided
    ]

    status_functions_enum = [
        has_no_sessions,
        has_session
    ]

    def _identify_led_color(self):
        # pylint: disable=invalid-name
        """Guesses self._led_color final value with all the instance's attributes"""
        status = None
        for func in self.status_functions:
            status = func(self)
            if status is not None:
                break
        if status or status == "":
            self.LedColor = status
            if self._need_to_reset:
                self._need_to_reset = False
                self._identify_led_color()
            return
        for func in self.status_functions_enum:
            status = func(self)
            if status is not None:
                break
        logging.info("Identified led status: %s", status.name)
        # Status is an enum
        self.LedColor = MachineState.status_color[status]

    def ForceColorSignal(self):
        # pylint: disable=invalid-name
        """Force the object to resend"""
        self._send_color_via_signal()

    def LedColorOverride(self, color):
        # pylint: disable=invalid-name
        """Sets the override color of the led, used by authorized users"""
        if not color or color.lower() == "reset":
            logging.info("Resetting the override color")
            self._led_color_overrided = None
            self._need_to_reset = True
        else:
            logging.info("Override color: %s", color)
            # A bit hacky but seems to work
            self.LedColor = ""
            self._led_color_overrided = color
        self._identify_led_color()

    # Methods for failure check
    def _lock_status_heartbeat_failure(self):
        """Check if i3lock has failed"""
        if self._sessions:
            for key, value in self._sessions.items():
                if not value.lock_ping_is_valid():
                    self.UnlockSession(key)
        return True # We need to return True if we want to keep the GLib hook

    PropertiesChanged = signal()

    status_color = {
        MachineStatus.POWER_OFF: "off",
        MachineStatus.POWER_ON: "green",
        MachineStatus.USER_LOGGED: "blue",
        MachineStatus.USER_LOCKED: "white",
        MachineStatus.USER_LOCKED_OVERTIME: "red",
        MachineStatus.COMPUTER_ISSUE_MEDIUM: "yellow",
        MachineStatus.COMPUTER_ISSUE_HIGH: "pink"
    }

    computer_health = {
        "unknown": MachineStatus.COMPUTER_HEALTHY,
        "low": MachineStatus.COMPUTER_ISSUE_LOW,
        "medium": MachineStatus.COMPUTER_ISSUE_MEDIUM,
        "high": MachineStatus.COMPUTER_ISSUE_HIGH
    }

def main():
    """Main function, gets the proper bus and publish MachineState"""
    sys_bus = SystemBus()
    my_state = MachineState(sys_bus)
    sys_bus.publish("org.cri.MachineState", my_state)

    # Resetting the LedColor if a previous color was already set
    my_state.LedColor = ""

    # Signal watchers
    sys_bus.subscribe("org.freedesktop.login1", "org.freedesktop.login1.Manager",
                      "SessionNew", "/org/freedesktop/login1", None, 0,
                      my_state.add_session_callback)
    sys_bus.subscribe("org.freedesktop.login1", "org.freedesktop.login1.Manager",
                      "SessionRemoved", "/org/freedesktop/login1", None, 0,
                      my_state.rm_session_callback)
    # Discover existing sessions
    my_state.discover_sessions()
    # Find existing issues
    my_state.get_issues()

    # Main Loop
    loop = GLib.MainLoop()
    loop.run()

if __name__ == "__main__":
    main()
