"""
Entrypoint for the machine state module
"""

from . import machine_state

def cmd_machine_state():
    """
    Entrypoint for the machine state module
    """
    machine_state.main()
