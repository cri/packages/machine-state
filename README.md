MachineStatePublisher will publish an DBus object containing metrics.

Usage for root:
```
COLOR='green'
dbus-send --system --print-reply --dest=org.cri.MachineState	\
	    /org/cri/MachineState                             	\
            org.cri.MachineState.LedColorOverride       	\
            string:"$COLOR"

```
